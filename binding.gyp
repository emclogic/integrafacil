{
    "targets": [
        {
            "include_dirs": [
                "futronic/include/",
                "<!(node -e \"require('nan')\")"
            ],
            "libraries": [
                "-lftrapi",
                "-lScanAPI",
                "-lusb-1.0",
                "-lpthread"
            ],
#            "cflags" : ["-m32"],
#            "ldflags" : ["-m32"],
             'cflags': ['-g'],
            'cflags!': [
                '-ansi'
            ],
            "target_name": "fingerAPI",
            "sources": [
                "finger.cpp"
            ]
        }
    ],
}
