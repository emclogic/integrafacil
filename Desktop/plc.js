const integrapi = require("integra-api");
const { ipcRenderer } = require('electron');

//console.log('Testing IntegraAPI v' + integrapi.version)
ipcRenderer.send('plc', "started plc");
var plc_info = integrapi.plc.Info();

document.getElementById("plc-info").innerHTML = "PLC Info:" + plc_info;
var toogle = 0;


function releTemp1() {
    var temp = document.getElementById("temp1").value;
    ipcRenderer.send('plc', temp);
    integrapi.plc.Rele1Temp(temp)
}

function releTemp2() {
    var temp = document.getElementById("temp2").value;
    ipcRenderer.send('plc', temp);
    integrapi.plc.Rele2Temp(temp)
}
