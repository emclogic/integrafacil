var os = require('os');
var ifaces = os.networkInterfaces();

var num = 1;
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function getIp() {
    while (1) {
        await sleep(1000);
        Object.keys(ifaces).forEach(function (ifname) {
            var alias = 0;
            ifaces[ifname].forEach(function (iface) {
                if ('IPv4' !== iface.family || iface.internal !== false) {
                    // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                    return;
                }

                if (alias >= 1) {
                    // this single interface has multiple ipv4 addresses
                    console.log(ifname + ':' + alias, iface.address);
                    document.getElementById("iplocal").innerHTML = "IP*:" + iface.address;

                } else {
                    // this interface has only one ipv4 adress
                    console.log(ifname, iface.address);
                    document.getElementById("iplocal").innerHTML = "IP*:" + iface.address;
                }
                ++alias;
            })
        } )
    }
}

getIp();
