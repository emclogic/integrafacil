const { ipcRenderer } = require('electron');
const request = require('request');
const integrapi = require("integra-api");

const http = require('http')

ip = '192.168.15.15:8080'

finger_version = '1.0'
document.getElementById("finger-version").innerHTML = "Biometria:" + finger_version;

function readFinger() {
    integrapi.finger.Enroll(function (data, err=0) {
        if (err) {
            console.log('Enroll Error:' + data);
            ipcRenderer.send('finger-ready', 'Enroll Error:' + data);
        } else {

            var result = '';
            result += data.toString();
            result = result.replace(/(\r\n|\n|\r)/gm, "");

            if(result) {

                url = 'http://' + ip + '/metodos/identificaTemplate?template=' + result
                ipcRenderer.send('finger-ready', result);
                request.get(url, {timeout: 1000}, function (err, res, body) {
                    if (err) {
                        ipcRenderer.send('finger-ready', 'Error server if offline');
                        document.getElementById("main-text").innerHTML =  'server is offline';
                    }
                    if (res.statusCode == 200) {
                        var d = JSON.parse(body)
                        document.getElementById("main-text").innerHTML =  d.resultado;
                        ipcRenderer.send('finger-ready', d.resultado + "\n" + result);
                    } else {
                        ipcRenderer.send('finger-ready', "server is offline");
                        document.getElementById("main-text").innerHTML =  'server is offline';
                    }
                })
            }
            readFinger();
        }
    });
}
readFinger();

console.log("Started with ip: " + ip);
ipcRenderer.send('finger-ready', "started finger");
