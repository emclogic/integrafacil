# Integra App

Rodar a aplicação com o seguinte comando: `npm start .`

Dicas:

Como acessar dispositivos USB sem usar sudo ou usuário root
https://askubuntu.com/questions/978552/how-to-make-libusb-work-as-non-root
https://stackoverflow.com/questions/40597515/libusb-calls-without-sudo-using-udev-rules

Debugar Electron com Visual studio code
https://github.com/Microsoft/vscode-recipes/tree/master/Electron