#include <nan.h>
#include <node.h>
#include <iostream>
#include <ftrapi.h>
#include <malloc.h>
#include <unistd.h>

using namespace std;
using namespace v8;
using namespace node;

#define VERSION 1.0

#define TEMP_SIZE 669

FTRAPI_RESULT _Res = FTR_RETCODE_OK;
int error_flag = 0;

#define CHECK_FTR_RESULT(res, text)               \
    if ((_Res = res) != FTR_RETCODE_OK)           \
    {                                             \
        printf("Error %d when %s\n", _Res, text); \
        error_flag = 1; \
    }

int _cancel = FTR_CONTINUE;

void FTR_CBAPI cbControl(FTR_USER_CTX Context, FTR_STATE StateMask, FTR_RESPONSE *pResponse,
                         FTR_SIGNAL Signal, FTR_BITMAP_PTR pBitmap)
{

    *pResponse = FTR_CONTINUE;

    if (_cancel == FTR_CANCEL) {
        *pResponse = FTR_CANCEL;
        cout << "Enroll cancelled by user" << endl;
    }


    if ((StateMask & FTR_STATE_SIGNAL_PROVIDED))
    {
        switch (Signal)
        {
        case FTR_SIGNAL_TOUCH_SENSOR:
            cout << "Put your finger on the scanner." << endl;
            break;

        case FTR_SIGNAL_TAKE_OFF:
            cout << "Take off your finger from the scanner." << endl;
            break;

        case FTR_SIGNAL_FAKE_SOURCE:
            cout << "Fake Finger Detected." << endl;
            *pResponse = FTR_CANCEL;
            break;

        default:
            printf("Bad signal value\n");
            break;
        }
    }
}

class Finger : public Nan::ObjectWrap
{
  public:
    static void Initialize(Local<Object> target);
    void close();

  private:
    Finger();
    ~Finger() { close(); }

    static NAN_METHOD(New);
    static NAN_METHOD(enrollSync);
    static NAN_METHOD(enroll);
    static NAN_METHOD(close);
    static NAN_METHOD(serial);
    static NAN_METHOD(info);
    static NAN_METHOD(cancel);

    static void WorkAsync(uv_work_t *req);
    static void WorkAsyncComplete(uv_work_t *req);

    static char* enroll();

    struct Worker {
        Worker(Finger *finger, Nan::Callback *callback)
            : _finger(finger),
              _callback(callback) {}

        ~Worker() {}

        Finger *_finger;
        Nan::Callback *_callback;
        vector<unsigned char> _data;
    };

    void readResultsToJSCallbackArguments(Worker *worker, Local<Value> argv[]);
};

Finger::Finger()
{
    cout << "Constructor" << endl;

    FTR_VERSION CompVersion;

    FTRTerminate();

    CHECK_FTR_RESULT(FTRInitialize(), "init");
    CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_CB_FRAME_SOURCE, (FTR_PARAM_VALUE)FSD_FUTRONIC_USB), "set image source");
    if (error_flag)
    {
        cout << "Error" << endl;
    }
    else
    {
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_CB_CONTROL, (FTR_PARAM_VALUE)cbControl), "set user callback function");
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_MAX_FARN_REQUESTED, (FTR_PARAM_VALUE)166), "set max FARN");
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_FAKE_DETECT, (FTR_PARAM_VALUE)FALSE), "set FAKE mode");
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_FFD_CONTROL, (FTR_PARAM_VALUE)TRUE), "set fake finger detected mode");
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_MIOT_CONTROL, (FTR_PARAM_VALUE)FALSE), "set MIOT mode");
        CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_MAX_MODELS, (FTR_PARAM_VALUE)1), "set number of templates");

        if (FTRGetParam(FTR_PARAM_VERSION, (FTR_PARAM_VALUE *)&CompVersion) == FTR_RETCODE_OK)
        {
            CHECK_FTR_RESULT(FTRSetParam(FTR_PARAM_VERSION, (FTR_PARAM_VALUE)FTR_VERSION_CURRENT), "set current version");
        }
        else
        {
            cout << "Version param not supported" << endl;
        }
    }
}

void Finger::close()
{
    Nan::HandleScope scope;
    cout << "Close" << endl;
}

char* Finger::enroll()
{
    FTR_DATA enrollSample;

    if (error_flag) {
        return "Error! could not init finger";
    }
    CHECK_FTR_RESULT(FTRGetParam(FTR_PARAM_MAX_TEMPLATE_SIZE, (FTR_PARAM_VALUE *)&enrollSample.dwSize), "get template size");

    cout << "Template Size: " << enrollSample.dwSize << endl;

    // Blocking Operation
    enrollSample.pData = malloc(enrollSample.dwSize);
    CHECK_FTR_RESULT(FTREnroll(NULL, FTR_PURPOSE_ENROLL, &enrollSample), "enroll base template");

    char userID[20];
    char fingerIdx[2];
    char templateSize[4];

    memset(userID, 0, sizeof(userID));
    memset(fingerIdx, 0, sizeof(fingerIdx));
    memset(templateSize, 0, sizeof(templateSize));

    sprintf(templateSize, "%d", enrollSample.dwSize);
    sprintf(fingerIdx, "%d", 1);
    sprintf(userID, "%d", 101);

    char *preBase64 = (char *)malloc(26 + enrollSample.dwSize);
    memcpy(preBase64, fingerIdx, sizeof(fingerIdx));
    memcpy(preBase64 + sizeof(fingerIdx), userID, sizeof(userID));
    memcpy(preBase64 + sizeof(fingerIdx) + sizeof(userID), templateSize, sizeof(templateSize));

    memcpy(preBase64 + sizeof(fingerIdx) + sizeof(userID) + sizeof(templateSize),
           enrollSample.pData, enrollSample.dwSize);

    return preBase64;

}

void Finger::readResultsToJSCallbackArguments(Worker* worker, Local<Value> argv[])
{
    const vector<unsigned char>& message = worker->_data;

    Local<Object> buf = Nan::NewBuffer(message.size()).ToLocalChecked();
    char* data = Buffer::Data(buf);
    
    int j = 0;
    for (vector<unsigned char>::const_iterator k = message.begin(); k != message.end(); k++) {
      data[j++] = *k;
    }
    argv[1] = buf;

}

void Finger::WorkAsync(uv_work_t* req)
{
    cout << "Worker Started" << endl;
    Worker *worker = static_cast<Worker *>(req->data);
    char *buf = enroll();

//    worker->_data = vector<unsigned char>(buf, buf + strlen(buf));
    worker->_data = vector<unsigned char>(buf, buf + TEMP_SIZE);
}

void Finger::WorkAsyncComplete(uv_work_t *req)
{
    Nan::HandleScope scope;
    Worker *worker = static_cast<Worker *>(req->data);

    Local<Value> argv[2];
    argv[0] = Nan::Undefined();
    argv[1] = Nan::Undefined();

    worker->_finger->readResultsToJSCallbackArguments(worker, argv);
    worker->_finger->Unref();

    Nan::AsyncResource resource("node-finger WorkAsyncComplete");
    worker->_callback->Call(2, argv, &resource);

    delete req;
    delete worker->_callback;
    delete worker;
}

NAN_METHOD(Finger::New)
{
    Nan::HandleScope scope;
    cout << "Finger New" << endl;

    //    try {
    Finger *finger;
    finger = new Finger();

    //    }
    finger->Wrap(info.This());
    info.GetReturnValue().Set(info.This());
}

NAN_METHOD(Finger::enroll)
{
  Nan::HandleScope scope;
  _cancel = FTR_CONTINUE;

  if (info.Length() != 1
      || !info[0]->IsFunction()) {
    return Nan::ThrowError("need one callback function argument in read");
  }

  Finger* finger = Nan::ObjectWrap::Unwrap<Finger>(info.This());
  finger->Ref();

  uv_work_t* req = new uv_work_t;
  req->data = new Worker(finger, new Nan::Callback(Local<Function>::Cast(info[0])));;
  uv_queue_work(uv_default_loop(), req, WorkAsync, (uv_after_work_cb)WorkAsyncComplete);

   return;

}

NAN_METHOD(Finger::cancel)
{
  Nan::HandleScope scope;
  _cancel = FTR_CANCEL;
}

NAN_METHOD(Finger::serial)
{
  Nan::HandleScope scope;
  Finger* finger = Nan::ObjectWrap::Unwrap<Finger>(info.This());
  finger->Ref();

  Local<Object> deviceInfo = Nan::New<Object>();

  const char s[] = "00000111";
  Nan::Set(deviceInfo, Nan::New<String>("serial").ToLocalChecked(),
           Nan::New<String>(s).ToLocalChecked());
  info.GetReturnValue().Set(deviceInfo);
}

NAN_METHOD(Finger::info)
{
  Nan::HandleScope scope;
  Finger* finger = Nan::ObjectWrap::Unwrap<Finger>(info.This());
  finger->Ref();

  Local<Object> deviceInfo = Nan::New<Object>();

  const char s[] = "Finger V1.0";
  Nan::Set(deviceInfo, Nan::New<String>("info").ToLocalChecked(),
           Nan::New<String>(s).ToLocalChecked());
  info.GetReturnValue().Set(deviceInfo);
}


NAN_METHOD(Finger::enrollSync)
{
    Nan::HandleScope scope;

    Finger* finger = Nan::ObjectWrap::Unwrap<Finger>(info.This());
    Local<Object> retVal = Nan::New<Object>();

    char *ret = enroll();

    retVal->Set(Nan::New<String>("template").ToLocalChecked(),
                Nan::New<String>(ret).ToLocalChecked());

    info.GetReturnValue().Set(retVal);
}

NAN_METHOD(Finger::close)
{
    Nan::HandleScope scope;
    cout << "Finger close" << endl;
}


static void
deinitialize(void *)
{
    cout << "Deinitialize" << endl;
}

void Finger::Initialize(Local<Object> target)
{
    cout << "Initialization" << endl;

    node::AtExit(deinitialize, 0);

    Nan::HandleScope scope;

    Local<FunctionTemplate> fingerTemplate = Nan::New<FunctionTemplate>(Finger::New);
    fingerTemplate->InstanceTemplate()->SetInternalFieldCount(1);
    fingerTemplate->SetClassName(Nan::New<String>("Finger").ToLocalChecked());

    Nan::SetPrototypeMethod(fingerTemplate, "close", close);
    Nan::SetPrototypeMethod(fingerTemplate, "enrollSync", enrollSync);
    Nan::SetPrototypeMethod(fingerTemplate, "enroll", enroll);
    Nan::SetPrototypeMethod(fingerTemplate, "serial", serial);
    Nan::SetPrototypeMethod(fingerTemplate, "info", info);
    Nan::SetPrototypeMethod(fingerTemplate, "cancel", cancel);
    //Nan::SetPrototypeMethod(fingerTemplate, "enrollAsync", enrollAsync);
    target->Set(Nan::New<String>("Finger").ToLocalChecked(), fingerTemplate->GetFunction());

}

extern "C" {

static void init(Local<Object> target)
{
    Nan::HandleScope scope;

    Finger::Initialize(target);
}

NODE_MODULE(Finger, init);
}
