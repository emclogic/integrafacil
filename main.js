const {app, BrowserWindow, ipcMain} = require('electron')
const integrapi = require("integra-api")
var express = require('express')

require('electron-reload')(__dirname)

console.log("IntegrAPI: " + integrapi.version);
ipcMain.on("finger-ready", (event, arg) => {
  console.log(arg);
})

ipcMain.on("plc", (event, arg) => {
  console.log(arg);
})

let mainWindow

function createWindow () {
    mainWindow = new BrowserWindow({width: 480, height:272, center:true, frame: false, kiosk: false})
    //  mainWindow = new BrowserWindow({width: 480, height:272, frame: true, kiosk: false})
    //mainWindow.loadFile('Desktop/index.html')
    mainWindow.loadURL(`file://${__dirname}/Desktop/index.html`);
    mainWindow.on('closed', function () {
        mainWindow = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
    app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

var webinterface = express()
webinterface.use(express.static('Web'))

webinterface.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'))
})


webinterface.listen(3000, function () {
  console.log('IntegraApp listening on port 3000!')
});

/* Leitura constante da reposta do PLC */
integrapi.plc.Read(function read(data, err = 0) {
    if (err) {
        console.log('Read Error:' + data);
    } else {
        if (data) console.log('data:' + data);
    }
})
